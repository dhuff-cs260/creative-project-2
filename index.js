/**
 * fetches a color scheme from the api based on the
 * data provided from the form on index.html
 */
function getColors() {
	let baseColor = document.getElementById("base-color").value;
	if (baseColor[0] === '#')
		baseColor = baseColor.substring(1);
	const relationship = document.getElementById("relationship-selector").value;
	const numColors = document.getElementById('num-colors').value;

	fetch(`https://www.thecolorapi.com/scheme?hex=${baseColor || 'F7CAC9'}&mode=${relationship}&count=${numColors}`)
		.then(async function(response) {
			const data = await response.json();
			const colors = [];
			data.colors.forEach(color => {
				let newColor = {hex: color['hex'].value, name: color['name'].value};
				colors.push(newColor);
			});
			
			shuffleArray(colors);
			displayColors(colors);
		})
		.catch(function(error) {
			console.log(error);
		})
}

/**
 * helper function for getColors that generates the html to display based on the response from the api
 * @param {Array} colors — an array of hex strings representing colors generated by the api
 */
function displayColors(colors) {
	let html = ``;
	colors.forEach(color => {
		html += `
			<div class="bg-base-100 relative">
				<div
					onclick="copyColor(this)"
					id=${color.hex}
					style="background-color: ${color.hex}"
					class="rounded-xl h-64 w-64 drop-shadow-xl"
				></div>
				<div class="absolute top-2 right-2 tooltip tooltip-left tooltip-secondary" data-tip="${color.hex}">
					<img
						class="w-6 h-6"
						src="https://cdn-icons-png.flaticon.com/512/4024/4024457.png"
						alt="copy"
						onclick="copyColor(this)">
				</div>
				<div id=${color.hex} class="text-xl absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2" onclick="copyColor(this)">${color.hex}</div>
				<div id=${color.hex} class="text-md text-center absolute top-1/3 left-1/2 transform -translate-x-1/2 -translate-y-1/2" onclick="copyColor(this)">${color.name}</div>
			</div>
		`;
	});
	document.getElementById('schemeContainer').innerHTML = html;
}

function copyColor(element) {
	const color = element.id;
	navigator.clipboard.writeText(color).then(function() {
		document.getElementById('copied-modal').checked = true;
	}, function(err) {
		console.error('Could not copy text: ', element.id, err);
	});
}

function shuffleArray(array) {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
}

function updatePreview() {
	let color = document.getElementById('base-color').value;
	if (color && color[0] !== '#') {
		color = `#${color}`;
	}
	document.getElementById('base-color-preview').style.backgroundColor = color;
}
